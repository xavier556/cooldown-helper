const Discord = require('discord.js');
const client = new Discord.Client();
const { prefix, token } = require('./config.json');
const Sequelize = require('sequelize');

// connection setup
const sequelize = new Sequelize('database', 'user', 'password', {
	host: 'localhost',
	dialect: 'sqlite',
	logging: false,
	// SQLite only
	storage: 'database.sqlite',
});

// database shema
const Category = sequelize.define('category', {
	name: {
		type: Sequelize.STRING,
		unique: true
	},
	message_id: {
		type: Sequelize.STRING,
		allowNull: false
	},
	cooldown: {
		type: Sequelize.INTEGER,
		allowNull: false
	},
	author: {
		type: Sequelize.STRING,
		allowNull: false
	}
});

const Player = sequelize.define('player', {
	name: {
		type: Sequelize.STRING
	},
	category: {
		type: Sequelize.STRING,
		allowNull: false
	},
	reset: {
		type: Sequelize.DATE,
		allowNull: false
	}
});

client.once('ready', () => {
	console.log('Ready!');
	// sync database
	Category.sync();
	Player.sync();
});

client.on('messageReactionAdd', async (reaction, user) => {
	if (reaction.emoji.name === "😀" && user.username != "Cooldown Helper") {

		let messageId = reaction.message.id;
		let username = user.username;

		// retrieve category from message id
		const cat = await Category.findOne({ where: { message_id: messageId } });
		if (cat) {

			// craete the user if it does not exist yet
			let p = await Player.findOne({ where: { name: username, category: cat.name } });
			if (p === null) {
				p = await Player.create({ name: username, category: cat.name, reset: new Date() });
			}
			else {
				p.set('reset', new Date());
				await p.save();
			}

			// update the message content
			getMessageEmbed(cat).then(embed => {
				reaction.message.edit(embed);
			});
		}
	}
});

client.on('message', async message => {

	if (message.content.startsWith(prefix)) {

		const input = message.content.slice(prefix.length).split(' ');
		const command = input.shift();

		if (command === 'cdh-add') {

			// First argument is the name of the category and must be present.
			const categoryName = input.shift();
			if (!categoryName) { return; }

			// Following argument is the timer and must be present.
			const timer = parseInt(input.shift());
			if (!Number.isInteger(timer)) { return; }

			message.channel.send('new category').then(m => {

				Category.create({
					name: categoryName,
					message_id: m.id,
					cooldown: timer,
					author: message.author.username
				}).then(data => {

					const cat = data.get();
					getMessageEmbed(cat).then(embed => {
						m.edit(embed);
						m.react('😀');
					});
				});
			});
		}
		else if (command === 'cdh-set-title') {
			const id = input.shift();
			const title = input.join(' ');
			if (!id || !title) return;
			const cat = await Category.findOne({ where: { message_id: id } });
			if (cat === null) return;
			cat.set({ name: title });
			await cat.save();
			// update the message content
			getMessageEmbed(cat).then(embed => {
				message.channel.messages.fetch(id).then(m => m.edit(embed));
			});
		}

		message.delete();
	}
});

client.login(token);


async function getMessageEmbed(category) {

	const players = await Player.findAll({ where: { category: category.name } });

	const cooldown = minutesToString(category.cooldown);

	const names = [];
	const resets = [];
	const ends = [];

	const embed = new Discord.MessageEmbed()
		.setTitle(category.name)
		.setThumbnail('https://wow.zamimg.com/images/wow/icons/large/trade_alchemy.jpg')
		.setColor('#0000ff')
		.setAuthor(category.author)
		.addField('Cooldown', cooldown, true)
		.addField('ID', category.message_id, true)
		.addField('\u200b', '\u200b');

	players.forEach(p => {
		names.push(`**${p.name}**`);
		resets.push(formatDate(p.reset));
		ends.push(formatDate(addMinutes(p.reset, category.cooldown)));
	});

	embed.addField('Players   ', names.join('\n') || '-', true)
		.addField('Last Reset On   ', resets.join('\n') || '-', true)
		.addField('Ends On   ', ends.join('\n') || '-', true)

	return embed;
}

function minutesToString(n) {
	let h = Math.floor(n / 60);
	let m = n % 60;

	if (h < 24) {
		return `${h}h ${m}m`;
	}
	else {
		let d = Math.floor(h / 24);
		h = h % 24;
		return `${d}d ${h}h ${m}m`;
	}
}

function addMinutes(date, minutes) {
	const d = new Date(date.getTime() + minutes * 60000);
	return d;
}

function formatDate(d) {
	return d.toLocaleString('fr-FR');
}